require 'mechanize'
require 'logger'
require 'redis'
require 'json'
require 'dotenv'

def push(redis, value)
  # puts "pushing #{value[3]}"
  redis.lpush("webqueue", value.to_json)  
end

def pop(redis)  
  retrieved_json = redis.lpop("webqueue")
  
  return if retrieved_json.nil?

  retrieved_array = JSON.parse(retrieved_json)
  # puts "popped #{retrieved_array[3]}"
  return retrieved_array
end

def add_visited(redis, hash)
  # puts "adding #{hash} to visited"
  redis.set(hash, 'visited')
end

def check_visited(redis, hash)
  # puts "checking #{hash} in visited"
  exists = redis.exists(hash)
  # puts "exists #{exists}"
  return exists == 1
end

# Load environment variables
Dotenv.load


logger = Logger.new 'crawler.log'
logger.level = Logger::WARN

redis_queue = Redis.new(host: ENV['REDIS_HOST'], port: ENV['REDIS_PORT'], db: 0, password: ENV['REDIS_PASSWORD'], ssl: true)
redis_visited = Redis.new(host: ENV['REDIS_HOST'], port: ENV['REDIS_PORT'], db: 1, ENV['REDIS_PASSWORD'], ssl: true)



input_website = ARGV[0]
root_website_uri = URI.parse(input_website)
root_host = "#{root_website_uri.scheme}://#{root_website_uri.host}"

# Set the starting URL
# url, from, title, hash
start_url = [input_website, '', 'Docs Gitlab', input_website]

# Add the starting URL to the queue
push(redis_queue, start_url)
still_items = true




# queue = [start_url]
i = 0

# Initialize a new Mechanize agent
agent = Mechanize.new
agent.read_timeout = 5


# Start the crawling loop
while still_items do
  i = i+1
  # Get the next URL from the queue
  parent_link = pop(redis_queue)

  if parent_link.nil?
    puts "end of graph"
    still_items = false
    next
  end

  url = parent_link[0]
  # puts "url to inspect: #{url}"

  next if check_visited(redis_visited, parent_link[3])
  add_visited(redis_visited, parent_link[3])  

  # Get the page at the current URL
  error = ""
  begin
    page = agent.get(url)
  rescue Mechanize::ResponseCodeError => e
    # | code | url | text | from | 
    next if e.response_code == "503"

    # remove line breaks from the link text
    link_text = parent_link[2]    
    link_text = link_text.gsub("\n", " ")

    error = "| #{e.response_code} | #{url} | #{link_text} | #{parent_link[1]} |"
    next
  rescue Net::ReadTimeout => e
    error = "| 555 | #{url} | #{parent_link[2]} | #{parent_link[1]} | "    
    next
  rescue => e    
    error = "| 000 | #{url} | #{parent_link[2]} | #{parent_link[1]} | "    
  end

  # check whether if error is not empty
  if !error.nil? && !error.empty?  
    logger.error(error)
    puts error
  else
    puts "OK: #{page.uri.to_s}"
  end
  
  # Search for links on the page    
  next if !page.uri.to_s.start_with?(start_url[0])  
  next if !page.is_a?(Mechanize::Page) 


  links = page.links

  #he queue if it has not been visited
  links.each do |link|

    # # relative
    href = link.href
    next if href.nil?
    if link.href.start_with?("/")
      href = root_host + href
    elsif link.href.start_with?("#")
      if page.uri.to_s.include? "#"
        href = page.uri.to_s.gsub(/\/#[\S]+/, href)        
      else
        href = page.uri.to_s.chomp("/") + link.href      
      end
      # puts "this is: #{href}"
    elsif !link.href.start_with?("http")
      begin
        href = URI.join(page.uri.to_s, link.href).to_s
      rescue
        href = link.href
      end
      # puts "pushing: #{href}"
    end
    
    hash = page.uri.to_s + "->" + href
    new_link = [ href, page.uri.to_s, link.text, hash ]

    push(redis_queue, new_link)
    # queue.push(new_link)
  end
end